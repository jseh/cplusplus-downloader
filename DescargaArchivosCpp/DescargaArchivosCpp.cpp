#include "pch.h"
//#include <tchar.h>
//#include <urlmon.h>
//#include <iostream>
//#include <fstream>
//#include <string>
//
//#pragma comment(lib, "urlmon.lib")
//
//int main()
//{
//	std::string line;
//	HRESULT hr;
//	hr = URLDownloadToFile(NULL, _T("https://raw.githubusercontent.com/downloader.go"), _T("temp.txt"), 0, NULL);
//	return 0;
//}

#include <windows.h>
#include <Urlmon.h>
#include <tchar.h>
#include <stdio.h>

#pragma comment(lib, "Urlmon.lib") /* remove if not using Visual Studio */

int main(void)
{
	TCHAR szUrl[] = _T("http://www.axmag.com/download/pdfurl-guide.pdf");
	TCHAR szFileName[] = _T("pdf.pdf");

	if (URLDownloadToFile(0, szUrl, szFileName, 0, 0) != S_OK)
	{
		printf("Windows error: %u", GetLastError());
		getchar();
		return -1;
	}

	printf("\nDownload ok. :)");

	ShellExecute(NULL, _T("open"), _T("calc.exe"), NULL, NULL, SW_SHOWDEFAULT);


	getchar();
	return 0;
}

//void startup(LPCSTR lpApplicationName)
//{
//	// additional information
//	STARTUPINFOA si;
//	PROCESS_INFORMATION pi;
//
//	// set the size of the structures
//	ZeroMemory(&si, sizeof(si));
//	si.cb = sizeof(si);
//	ZeroMemory(&pi, sizeof(pi));
//
//	// start the program up
//	CreateProcessA
//	(
//		lpApplicationName,   // the path
//		argv[1],                // Command line
//		NULL,                   // Process handle not inheritable
//		NULL,                   // Thread handle not inheritable
//		FALSE,                  // Set handle inheritance to FALSE
//		CREATE_NEW_CONSOLE,     // Opens file in a separate console
//		NULL,           // Use parent's environment block
//		NULL,           // Use parent's starting directory 
//		&si,            // Pointer to STARTUPINFO structure
//		&pi           // Pointer to PROCESS_INFORMATION structure
//	);
//	// Close process and thread handles. 
//	CloseHandle(pi.hProcess);
//	CloseHandle(pi.hThread);
//}